/*global require*/
'use strict';

require.config({
    shim: {
        bootstrap: {
            deps: ['jquery'],
            exports: 'jquery'
        }
    },
    paths: {
        jquery: '../bower_components/jquery/dist/jquery',
        backbone: '../bower_components/backbone/backbone',
        underscore: '../bower_components/lodash/dist/lodash',
        bootstrap: '../bower_components/bootstrap-sass-official/assets/javascripts/bootstrap'
    }
});

require([
    'backbone',
    'views/modal',
    'views/button',
    'views/header',
    'views/personForm',
    'views/personView',
    'views/panel'
], function (Backbone, Modal, Button, Header, PersonForm, PersonView, PanelView) {
    Backbone.history.start();

    // either put specialized buttons in forms or reuse generic buttons
    var GenericCancelButton = Button.extend({
      text: 'Cancel',
      click: function() {
        this.$el.trigger('hide.rb.modal');
        this.$el.trigger('cancel.rb.click');
      }
    });

    var modal = new Modal({ backdrop: 'static' });
    modal.mountComponents({
      header: (new Header({ title: 'The best person form, ever' })).render(),
      body: (new PersonForm()).render(),
      footer: (new GenericCancelButton()).render()
    });

    (new Button({
      text: 'Launch Modal',
      click: function() {
        modal.toggle().then(function() {
          console.log("consumers don't care about bootstrap events.");
        });
      },
    })).appendTo('#modal-button').render();

    (new Button({
      text: 'Launch Modal 2',
      click: function() {
        (new Modal()).mountComponents({
          body: (new PersonForm()).render()
        }).show();
      },
    })).appendTo('#modal-button-2').render();

    (new Button({
      text: 'Modal that closes itself in 5 seconds',
      click: function() {
        (new Modal()).mountComponents({
          body: (new PersonForm()).render()
        }).show({timeout: 5000});
      },
    })).appendTo('#modal-button-3').render();

    (new PersonView())
    .mountComponents({
      header: (new Header({ title: 'Inline Form' })).render(),
      body: (new PersonForm()).render(),
      footer: (new PanelView({className: 'btn-toolbar'})).mountComponents([
        (new Button({
          text: 'Save',
          className: 'btn btn-success btn-lg'
        })).render(),
        (new GenericCancelButton()).render(),
      ])
    })
    .appendTo('#edit-person');

});
