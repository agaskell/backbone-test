/*global define*/

define([
    'jquery',
    'underscore',
    'backbone'
], function ($, _, Backbone) {
    'use strict';

    var HeaderView = Backbone.View.extend({
        template: _.template('<%= title %>'),

        tagName: 'h4',

        className: 'modal-title',

        events: {},

        initialize: function (options) {
          this.title = options.title;
        },

        render: function () {
          this.$el.html(this.template({title: this.title}));
          return this;
        }
    });

    return HeaderView;
});
