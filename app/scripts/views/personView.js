define([
  'jquery',
  'underscore',
  'backbone',
  'templates'
], function ($, _, Backbone, JST) {
  'use strict';

  var PersonView = Backbone.View.extend({
    template: JST['app/scripts/templates/personView.ejs'],

    tagName: 'div',

    id: '',

    className: '',

    events: {},

    initialize: function (options) {
      //this.components = options.components;
    },

    mountComponents: function (mountables) {
      if (!this.rendered) { this.render(); }

      _.each(mountables, function (mountable, mountPoint) {

        var $el;

        if (mountable instanceof Backbone.View) {
          $el = mountable.$el;
        }
        else {
          $el = mountable;
        }

        this.mountPoints[mountPoint].html($el);
      }, this);

      return this;
    },

    appendTo: function (target) {
      this.$el.appendTo(target);
      return this;
    },

    render: function () {
      this.$el.html(this.template());

      var baseSelector = "> ";
      this.$header = this.$el.find(baseSelector + ".person-header");
      this.$body = this.$el.find(baseSelector + ".person-body");
      this.$footer = this.$el.find(baseSelector + ".person-footer");

      this.mountPoints = {
        "header": this.$header,
        "body": this.$body,
        "footer": this.$footer,
      };

      this.rendered = true;
      return this;
    },

  });

  return PersonView;
});
