/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var ButtonView = Backbone.View.extend({
        template: _.template('<%= text %>'),  //JST['app/scripts/templates/button.ejs'],

        tagName: 'button',

        type: 'button',

        id: '',

        className: 'btn btn-primary btn-lg',

        events: {'click': '_onClick'},

        initialize: function (options) {
          if(options) {
            this.click = options.click || this.click;
            this.id = options.id || this.id;
            this.text = options.text || this.text;
          }
        },

        render: function () {
          this.$el.html(this.template({
            id: this.id,
            text: this.text,
            type: this.type
          }));
          return this;
        },

        appendTo: function (target) {
          this.$el.appendTo(target);
          return this;
        },

        _onClick: function() {
          if(this.click) { this.click(); }
        }
    });

    return ButtonView;
});
