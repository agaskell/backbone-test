/*global define*/

define([
    'jquery',
    'underscore',
    'backbone',
    'templates'
], function ($, _, Backbone, JST) {
    'use strict';

    var PersonFormView = Backbone.View.extend({
        template: JST['app/scripts/templates/personForm.ejs'],

        tagName: 'form',

        id: '',

        className: '',

        events: {},

        initialize: function () {

        },

        render: function () {
          this.$el.html(this.template());
          return this;
        },
    });

    return PersonFormView;
});
