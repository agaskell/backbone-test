define(function (require) {
  "use strict";

  var $ = require("jquery");
  var _ = require("underscore");
  var Backbone = require("backbone");
  var JST = require("templates");
  var Bootstrap = require("bootstrap");

  var ModalView = Backbone.View.extend({

    template: JST["app/scripts/templates/modal.ejs"],

    events: {
      "show.rb.modal": "_doShow",
      "hide.rb.modal": "_doHide",
      "toggle.rb.modal": "_doToggle"
    },

    tagName: "div",
    className: "rb-modal modal",

    attributes: {
      "role": "dialog"
    },

    initialize: function (options) {
      options = options || {};

      _.extend(this, ModalView.defaults, options);

      this.rendered = false;
      this.visible = false;

      if (this.animate) {
        this.$el.addClass("fade");
      }
    },

    render: function () {
      this.$el.html(this.template());

      var baseSelector = ".modal-dialog > .modal-content > ";
      this.$header = this.$el.find(baseSelector + ".modal-header");
      this.$body = this.$el.find(baseSelector + ".modal-body");
      this.$footer = this.$el.find(baseSelector + ".modal-footer");

      this.mountPoints = {
        "header": this.$header,
        "body": this.$body,
        "footer": this.$footer,
      };

      //todo: this belongs in a base class
      _.each(this.mountPoints, function(mp) {
        if(mp.is('[data-hide-if-empty]')) {
          mp.addClass('hidden');
        }
      });

      this.$el.modal({
        backdrop: this.backdrop,
        keyboard: this.keyboard,
        show: this.showOnRender
      });

      this.visible = this.showOnRender;
      this.rendered = true;

      return this;
    },

    // autofocus attribute doesn't work with BS modals. calling this fixes that.
    _setAutofocus: function () {
      this.$el.find("input[autofocus],textarea[autofocus]")
      .first()
      .focus();
    },

    appendTo: function (target) {
      this.$el.appendTo(target);
      return this;
    },

    mountComponents: function (mountables) {
      if (!this.rendered) { this.render(); }

      _.each(mountables, function (mountable, mountPoint) {

        var $el;

        if (mountable instanceof Backbone.View) {
          $el = mountable.$el;
        }
        else {
          $el = mountable;
        }

        // also belongs in a base class
        var mp = this.mountPoints[mountPoint];
        if(mp.is('[data-hide-if-empty')) {
          mp.removeClass('hidden');
        }

        mp.html($el);
      }, this);

      this.$el.modal("handleUpdate");

      return this;
    },

    show: function (options) {
      var deferred = $.Deferred();

      if (!this.rendered) {
        this.render();
      }

      var self = this;

      this.$el.one("shown.bs.modal", function () {
        self._setAutofocus();
        deferred.resolve();
      });

      this.$el.modal("show");
      this.visible = true;

      if(options && options.timeout) {
        setTimeout(function() { self.hide(); }, options.timeout);
      }

      return deferred.promise();
    },

    hide: function () {
      var deferred = $.Deferred();

      if (!this.rendered) {
        this.render();
      }

      this.$el.one("hidden.bs.modal", function () {
        deferred.resolve();
      });

      this.$el.modal("hide");
      this.visible = false;

      return deferred.promise();
    },

    toggle: function () {
      var deferred = $.Deferred();

      if (!this.rendered) {
        this.render();
      }

      var modal = this.$el.data("bs.modal");

      var self = this;
      this.$el.one(modal.isShown ?
        "hidden.bs.modal" :
        "shown.bs.modal",
      function () {
        self._setAutofocus();
        deferred.resolve();
      });

      this.$el.modal("toggle");
      this.visible = false;

      return deferred.promise();
    },

    destroy: function () {
      this.$el.modal("removeBackdrop");
      this.$el.data("bs.modal", null);
      Backbone.View.prototype.remove.call(this);
      this.rendered = false;
      this.visible = false;
    },

    _doShow: function (e) {
      e.stopPropagation();
      this.show();
    },

    _doHide: function (e) {
      e.stopPropagation();
      this.hide();
    },

    _doToggle: function (e) {
      e.stopPropagation();
      this.toggle();
    }

  }, {
    defaults: {
      animate: true,
      backdrop: true,
      keyboard: true,
      showOnRender: false
    }
  });

  return ModalView;
});